﻿/// <binding ProjectOpened='watch' />
/*
This file in the main entry point for defining Gulp tasks and using Gulp plugins.
Click here to learn more. http://go.microsoft.com/fwlink/?LinkId=518007
*/

var gulp = require("gulp"),
    del = require("del"),
    uglify = require("gulp-uglify"),
    rename = require("gulp-rename"),
    runSequence = require('run-sequence'),
    gutil = require('gulp-util'),
    gzip = require('gulp-gzip'),
    docma = require('docma');

// Command line option:
//  --fatal=[warning|error|off]
var fatalLevel = require('yargs').argv.fatal;

var ERROR_LEVELS = ['error', 'warning'];

// Return true if the given level is equal to or more severe than
// the configured fatality error level.
// If the fatalLevel is 'off', then this will always return false.
// Defaults the fatalLevel to 'error'.
function isFatal(level) {
    return ERROR_LEVELS.indexOf(level) <= ERROR_LEVELS.indexOf(fatalLevel || 'error');
}

// Handle an error based on its severity level.
// Log all levels, and exit the process for fatal levels.
function handleError(level, error) {
    gutil.log(error.message);
    if (isFatal(level)) {
        process.exit(1);
    }
    this.emit('end');
}

// Convenience handler for error-level errors.
function onError(error) { handleError.call(this, 'error', error); }
// Convenience handler for warning-level errors.
function onWarning(error) { handleError.call(this, 'warning', error); }

gulp.task('watch', function () {
    fatalLevel = fatalLevel || 'off';
    gulp.watch('fecha.js', ['min:js']);
});

gulp.task("min:js", function () {
    return gulp.src(["fecha.js"])
    .pipe(uglify().on('error', onWarning))
    .pipe(rename("fecha.min.js"))
    .pipe(gulp.dest('.'))
    .pipe(gzip())
    .pipe(gulp.dest('.'));
});

gulp.task("buildDoc", function () {
    docma.create()
    .build({
        src: [{ 'fecha': ['fecha.js'] }],
        dest: './doc',
        debug: 31,
        app: {
            title: 'Fecha Documentation',
            routing: 'query',
            base: '/fecha/doc',
            entrance: 'api:fecha'
        },
    })
    .catch(function (error) {
        console.log(error);
    });
});

gulp.task("build", ["min:js"]);

gulp.task('clean', function () {
    return del.sync('fecha.min.js');
});